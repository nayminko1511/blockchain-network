/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

public protocol BlockchainNetworkDelegate: class {
    func blockchainNetworkBrowseResultsDidUpdate()
    func blockchainPeerNodesDidUpdate()
}

internal protocol BlockchainNetworkProtocol {
    func startListening()
    func stopListening()
    func startBrowsing()
    func stopBrowsing()
    func connectToPeerNodes()
    var servers: Set<PeerNode> { get }
    var clients: Set<PeerNode> { get }
    init(delegate: BlockchainNetworkDelegate?, peerNodeDelegate: PeerNodeDelegate?)
}

public class BlockchainNetwork: BlockchainNetworkProtocol {
    public var delegate: BlockchainNetworkDelegate?
    public var peerNodeDelegate: PeerNodeDelegate?
    public let nodeId: String
    
    /* Custom implementation */
    private var listener: PeerListener!
    private var browser = PeerBrowser()
    private var serverNodes = Set<PeerNode>()
    private var clientNodes = Set<PeerNode>()
    
    public required init(delegate: BlockchainNetworkDelegate?, peerNodeDelegate: PeerNodeDelegate?) {
        self.delegate = delegate
        self.peerNodeDelegate = peerNodeDelegate
        nodeId = UUID().uuidString
        listener = PeerListener(nodeId: nodeId)
        listener.peerConnectionDelegate = self
        browser.delegate = self
    }
    
    public var servers: Set<PeerNode> {
        return serverNodes
    }
    
    public var clients: Set<PeerNode> {
        return clientNodes
    }
    
    public func startListening() {
        listener.startListening()
    }
    
    public func stopListening() {
        listener.stopListening()
    }
    
    public func startBrowsing() {
        browser.startBrowsing()
    }
    
    public func stopBrowsing() {
        browser.browser!.cancel()
    }
    
    public func connectToPeerNodes() {        
        for result in browser.browser.browseResults {
            if (!result.interfaces.contains { $0.type == .loopback }) {
                switch result.metadata {
                case .bonjour(let txtRecord):
                    if let peerNodeId = UUID(uuidString: txtRecord.dictionary["node"]!)?.uuidString {
                        print("TXT Record \(txtRecord.dictionary)")
                        if !serverNodes.contains(where: { $0.nodeId == peerNodeId }) {
                            print("\(result.endpoint) being connected")
                            let _ = PeerConnection.init(endpoint: result.endpoint, interface: result.endpoint.interface, peerNodeId: peerNodeId, delegate: self)
                        } else {
                            print("\(result.endpoint) already connnnn")
                        }
                    } else {
                        print("Invalid UUID")
                    }
                default:
                    print("No TXT record")
                }
            } else {
                print("Loop back")
            }
        }
    }
}

/* Custom implementation */
extension BlockchainNetwork: PeerBrowserDelegate, PeerConnectionDelegate {
    func peerBrowserBrowseResultsDidUpdate(results: Set<NWBrowser.Result>, changes: Set<NWBrowser.Result.Change>) {
        delegate?.blockchainNetworkBrowseResultsDidUpdate()
    }
    
    func peerConnectionDidAddConnection(_ peerConnection: PeerConnection) {
        if peerConnection.isInitiatedConnection {
            serverNodes.insert(PeerNode(peerConnection: peerConnection))
        } else {
            clientNodes.insert(PeerNode(peerConnection: peerConnection))
        }
        delegate?.blockchainPeerNodesDidUpdate()
    }
    
    func peerConnectionDidRemoveConnection(_ peerConnection: PeerConnection) {
        if peerConnection.isInitiatedConnection {
            serverNodes.remove(PeerNode(peerConnection: peerConnection))
        } else {
            clientNodes.remove(PeerNode(peerConnection: peerConnection))
        }
        delegate?.blockchainPeerNodesDidUpdate()
    }
    
    func peerConnectionDidReceiveMessage(peerConnection: PeerConnection, content: Data?, message: NWProtocolFramer.Message) {
        guard let data = content else {
            fatalError()
        }
        peerNodeDelegate?.peerNode(PeerNode(peerConnection: peerConnection), didReceiveMessage: BlockchainMessage(messageType: message.blockchainMessageType, data: data))
    }
}

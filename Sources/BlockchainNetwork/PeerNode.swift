/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

internal protocol PeerNodeProtocol {
    func send(blockchainMessage: BlockchainMessage)
    func stopConnection()
    var nodeId: String { get }
    var isServer: Bool { get }
}

public protocol PeerNodeDelegate: class {
    func peerNode(_ peerNode: PeerNode, didReceiveMessage message: BlockchainMessage)
}

public class PeerNode: PeerNodeProtocol, Hashable {
    public weak var delegate: PeerNodeDelegate?
    
    private var connection: PeerConnection
    
    public static func == (lhs: PeerNode, rhs: PeerNode) -> Bool {
        return lhs.connection == rhs.connection
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(connection)
    }
    
    public func send(blockchainMessage: BlockchainMessage) {
        let message = NWProtocolFramer.Message(blockchainMessageType: blockchainMessage.messageType)
        let context = NWConnection.ContentContext(identifier: String(describing: blockchainMessage.messageType), metadata: [message])
        connection.connection.send(content: blockchainMessage.data, contentContext: context, isComplete: true, completion: .idempotent)
    }
    
    /* Custom implementation */
    init(peerConnection: PeerConnection) {
        self.connection = peerConnection
    }
    
    public func stopConnection() {
        connection.stopConnection()
    }
    
    public var nodeId: String {
        return connection.peerNodeId
    }
    
    public var isServer: Bool {
        return connection.isInitiatedConnection
    }
}

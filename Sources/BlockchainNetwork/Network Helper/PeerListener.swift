/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * This code is derived from sample software by Apple Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

let serviceType = "_blockchain._tcp"

class PeerListener {

	weak var peerConnectionDelegate: PeerConnectionDelegate?
	weak var listener: NWListener!
    private let nodeId: String
    
	// Create a listener with a name to advertise, a passcode for authentication,
	// and a delegate to handle inbound connections.
    
    init(nodeId: String) {
        self.nodeId = nodeId
    }

	// Start listening and advertising.
	func startListening() {
		do {
			// Create the listener object.
			let listener = try NWListener(using: NWParameters())
            self.listener = listener

			// Set the service to advertise.
            listener.service = NWListener.Service(name: nodeId, type: serviceType, txtRecord: NWTXTRecord(["node" : nodeId]))

			listener.stateUpdateHandler = { newState in
				switch newState {
				case .ready:
                    break
				case .failed(let error):
					if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_DefunctConnection)) {
						print("Listener failed with \(error), restarting")
						listener.cancel()
						self.startListening()
					} else {
						print("Listener failed with \(error), stopping")
						listener.cancel()
					}
				case .cancelled:
                    print("Listener \(self.nodeId) cancelled")
				default:
					break
				}
			}

			listener.newConnectionHandler = { newConnection in
                print("new connection from \(newConnection.endpoint)")
                let _ = PeerConnection(connection: newConnection, peerNodeId: UUID().uuidString, delegate: self.peerConnectionDelegate)
			}

			// Start listening, and request updates on the main queue.
			listener.start(queue: .main)
		} catch {
			print("Failed to create listener")
			abort()
		}
	}
    
    func stopListening() {
        listener.cancel()
    }
    
    deinit {
        print("PeerListener deinit")
        listener.cancel()
    }
}

/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * This code is derived from sample software by Apple Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

class BlockchainProtocol: NWProtocolFramerImplementation {
    static let definition = NWProtocolFramer.Definition(implementation: BlockchainProtocol.self)
    
    static var label: String {
        return "Blockchain"
    }
    
    required init(framer: NWProtocolFramer.Instance) {
        
    }
    
    func start(framer: NWProtocolFramer.Instance) -> NWProtocolFramer.StartResult {
        return .ready
    }
    
    func handleInput(framer: NWProtocolFramer.Instance) -> Int {
        while true {
            var tempHeader: BlockchainProtocolHeader?
            let parsed = framer.parseInput(minimumIncompleteLength: BlockchainProtocolHeader.size, maximumLength: BlockchainProtocolHeader.size) { (buffer, isComplete) -> Int in
                guard let buffer = buffer else {
                    return 0
                }
                if buffer.count < BlockchainProtocolHeader.size {
                    return 0
                }
                tempHeader = BlockchainProtocolHeader(from: buffer)
                return BlockchainProtocolHeader.size
            }
            
            guard parsed, let header = tempHeader else {
                return BlockchainProtocolHeader.size
            }
            
            var messageType = BlockchainMessageType.invalid
            if let type = BlockchainMessageType(rawValue: header.messageType) {
                messageType = type
            }
            let message = NWProtocolFramer.Message(blockchainMessageType: messageType)
            
            if !framer.deliverInputNoCopy(length: Int(header.length), message: message, isComplete: true) {
                return 0
            }
        }
    }
    
    func handleOutput(framer: NWProtocolFramer.Instance, message: NWProtocolFramer.Message, messageLength: Int, isComplete: Bool) {
        let messageType = message.blockchainMessageType
        let header = BlockchainProtocolHeader(type: messageType.rawValue, length: UInt32(messageLength))
        
        framer.writeOutput(data: header.encodedData)
        do {
            try framer.writeOutputNoCopy(length: messageLength)
        } catch {
            print("Error writing \(error.localizedDescription)")
        }
    }
    
    func wakeup(framer: NWProtocolFramer.Instance) {
        
    }
    
    func stop(framer: NWProtocolFramer.Instance) -> Bool {
        return true
    }
    
    func cleanup(framer: NWProtocolFramer.Instance) {
        
    }
}

struct BlockchainProtocolHeader: Codable {
    let messageType: UInt32
    let length: UInt32
    
    init(type: UInt32, length: UInt32) {
        self.messageType = type
        self.length = length
    }
    
    init(from buffer: UnsafeMutableRawBufferPointer) {
        messageType = UnsafeMutableRawBufferPointer(start: buffer.baseAddress, count: MemoryLayout<UInt32>.size).load(as: UInt32.self)
        length = UnsafeMutableRawBufferPointer(start: buffer.baseAddress!.advanced(by: MemoryLayout<UInt32>.size), count: MemoryLayout<UInt32>.size).load(as: UInt32.self)
    }
    
    var encodedData: Data {
        var tempMessageType = messageType
        var tempLength = length
        var data = Data(bytes: &tempMessageType, count: MemoryLayout<UInt32>.size)
        data.append(Data(bytes: &tempLength, count: MemoryLayout<UInt32>.size))
        return data
    }
    
    static let size = MemoryLayout<UInt32>.size * 2
}

extension NWProtocolFramer.Message {
    convenience init(blockchainMessageType: BlockchainMessageType) {
        self.init(definition: BlockchainProtocol.definition)
        self["BlockchainMessageType"] = blockchainMessageType
    }
    
    var blockchainMessageType: BlockchainMessageType {
        if let type = self["BlockchainMessageType"] as? BlockchainMessageType {
            return type
        } else {
            return BlockchainMessageType.invalid
        }
    }
}

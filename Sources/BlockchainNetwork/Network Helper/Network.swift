/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

func describeEndpoint(_ endpoint: NWEndpoint) {
    print("DESCRIBING Endpoint")
    switch endpoint {
    case .hostPort(host: let host, port: let port):
        print("Host host: \(host.debugDescription); port: \(port.debugDescription)")
    case .service(name: let name, type: let type, domain: let domain, interface: let interface):
        print("Service name: \(name); type: \(type); domain: \(domain); interface \(interface != nil ? interface!.debugDescription : "No interface")")
    case .unix(path: let path):
        print("Unix: \(path)")
    case .url(let url):
        print("URL: \(url)")
    @unknown default:
        break
    }
}

struct NodeNetwork {
    weak var delegate: BlockchainNetworkDelegate?
    private var establishedConnections = Set<PeerConnection>()
    
    func tellDelegate() {
        delegate?.blockchainNetworkBrowseResultsDidUpdate()
    }
    
    mutating func addConnection(_ connection: PeerConnection) {
        self.establishedConnections.insert(connection)
    }
    
    mutating func removeConnection(_ connection: PeerConnection) {
        establishedConnections.remove(connection)
    }
    
    mutating func removeAll() {
        establishedConnections.removeAll()
    }
    
    var connections: Set<PeerConnection> {
        get {
            return establishedConnections
        }
    }
    
    var initiatedConnections: Array<PeerConnection> {
        return establishedConnections.filter { $0.isInitiatedConnection }
    }
}

//var nodeNetwork = NodeNetwork()

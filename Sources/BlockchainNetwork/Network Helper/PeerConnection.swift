/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * This code is derived from sample software by Apple Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

protocol PeerConnectionDelegate: class {
    func peerConnectionDidAddConnection(_ peerConnection: PeerConnection)
    func peerConnectionDidRemoveConnection(_ peerConnection: PeerConnection)
    func peerConnectionDidReceiveMessage(peerConnection: PeerConnection, content: Data?, message: NWProtocolFramer.Message)
}

class PeerConnection: Hashable {
    static func == (lhs: PeerConnection, rhs: PeerConnection) -> Bool {
        return lhs.peerNodeId == rhs.peerNodeId
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(peerNodeId)
    }
    

    weak var delegate: PeerConnectionDelegate?
	weak var connection: NWConnection!
    var peerNodeId: String
	let isInitiatedConnection: Bool

	// Create an outbound connection when the user initiates a game.
    init(endpoint: NWEndpoint, interface: NWInterface?, peerNodeId: String, delegate: PeerConnectionDelegate?) {
        if delegate == nil {
            fatalError()
        }
		self.delegate = delegate
		self.isInitiatedConnection = true
        self.peerNodeId = peerNodeId

		let connection = NWConnection(to: endpoint, using: NWParameters())
		self.connection = connection

		startConnection()
	}

	// Handle an inbound connection when the user receives a game request.
    init(connection: NWConnection, peerNodeId: String, delegate: PeerConnectionDelegate?) {
        if delegate == nil {
            fatalError()
        }
		self.connection = connection
		self.isInitiatedConnection = false
        self.peerNodeId = peerNodeId
        self.delegate = delegate

		startConnection()
	}

	// Handle starting the peer-to-peer connection for both inbound and outbound connections.
	func startConnection() {
		connection.stateUpdateHandler = { newState in
			switch newState {
			case .ready:
                print("Established \(self.connection.endpoint)")
                self.delegate?.peerConnectionDidAddConnection(self)
                describeEndpoint(self.connection.endpoint)

				// When the connection is ready, start receiving messages.
				self.receiveNextMessage()

				// Notify your delegate that the connection is ready.
			case .failed(let error):
                print("\(self.connection.endpoint) failed with \(error)")

				// Cancel the connection upon a failure.
                self.connection.cancel()
                self.delegate?.peerConnectionDidRemoveConnection(self)
			case .cancelled:
                self.delegate?.peerConnectionDidRemoveConnection(self)
			default:
				print("Connection case default \(newState)")
				break
			}
		}

		// Start the connection establishment.
		connection.start(queue: .main)
	}
    
    func stopConnection() {
        connection.cancel()
        self.delegate?.peerConnectionDidRemoveConnection(self)
    }
    
    func receiveNextMessage() {
        guard let connection = connection else {
            print("No connection to receive message")
            return
        }
        
        connection.receiveMessage { (content, context, isComplete, error) in
            if let blockchainMessage = context?.protocolMetadata(definition: BlockchainProtocol.definition) as? NWProtocolFramer.Message {
                self.delegate!.peerConnectionDidReceiveMessage(peerConnection: self, content: content, message: blockchainMessage)
            }
            
            if error == nil {
                self.receiveNextMessage()
            }
        }
    }
    
    deinit {
        connection?.cancel()
        print("PeerConnection deinit")
    }

}

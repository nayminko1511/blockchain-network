/*-
 * BSD 3-Clause License
 *
 * Copyright (c) 2021, Nay Min Ko
 * All rights reserved.
 *
 * This code is derived from sample software by Apple Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

import Foundation
import Network

// Update the UI when you receive new browser results.

protocol PeerBrowserDelegate: class {
    func peerBrowserBrowseResultsDidUpdate(results: Set<NWBrowser.Result>, changes: Set<NWBrowser.Result.Change>)
}

class PeerBrowser {

    weak var delegate: PeerBrowserDelegate?
    weak var peerConnectionDelegate: PeerConnectionDelegate?
	var browser: NWBrowser!

	// Start browsing for services.
	func startBrowsing() {

		// Browse for a custom "_tictactoe._tcp" service type.
		let browser = NWBrowser(for: .bonjourWithTXTRecord(type: serviceType, domain: nil), using: NWParameters())
		self.browser = browser
		browser.stateUpdateHandler = { newState in
			switch newState {
			case .failed(let error):
				// Restart the browser if it loses its connection
				if error == NWError.dns(DNSServiceErrorType(kDNSServiceErr_DefunctConnection)) {
					print("Browser failed with \(error), restarting")
					browser.cancel()
					self.startBrowsing()
				} else {
					print("Browser failed with \(error), stopping")
					browser.cancel()
				}
			case .ready:
				print("Browser ready")
				// Post initial results.
			case .cancelled:
				print("Browser cancelled")
			default:
//				print("Browser \(nodeId) state: \(newState)")
				break
			}
		}

		// When the list of discovered endpoints changes, refresh the delegate.
		browser.browseResultsChangedHandler = { results, changes in
            self.delegate?.peerBrowserBrowseResultsDidUpdate(results: results, changes: changes)
		}

		// Start browsing and ask for updates on the main queue.
		browser.start(queue: .main)
	}
}

import XCTest

import BlockchainNetworkTests

var tests = [XCTestCaseEntry]()
tests += BlockchainNetworkTests.allTests()
XCTMain(tests)

import XCTest
@testable import BlockchainNetwork

final class BlockchainNetworkTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(BlockchainMessageType.invalid.rawValue, 0)
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
